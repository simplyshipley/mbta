<?php

const MBTA_API_KEY = '<API KEY GOES HERE>';

//TODO Enter your API Key above.

function call_mbta($endpoint) {

  // create curl resource
  $ch = curl_init();
  $url = 'https://api-v3.mbta.com' . $endpoint;
  
  // set url
  curl_setopt($ch, CURLOPT_URL, $url);
  curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    'x-api-key: ' . MBTA_API_KEY,
  ));
  //return the transfer as a string
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  
  // $output contains the output string
  $output = curl_exec($ch);
  
  // close curl resource to free up system resources
  curl_close($ch);
  
  return $output;
}

function get_routes($requested_routes) {
  $routes = [];
  foreach ($requested_routes as $key => $value) {
    $routes[$key]['route_data'] = call_mbta('/routes/' . $key);
  }
  return $routes;
}

// TODO filter routes down by type instead of a hard coded list.

$requested_routes = [
  'Red' => 'Red Line',
  'Orange' => 'Orange Line',
];

$routes = get_routes($requested_routes);

foreach ($requested_routes as $key => $value) {
  $routes[$key]['stops'] = call_mbta('/stops?filter[route]=' . $key);
//  $routes[$key]['schedule'] = call_mbta('/schedules?filter[route]=' . $key);
}

print_r($routes);
